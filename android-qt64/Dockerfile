FROM ubuntu:22.04

# inspired by rabits/qt which we use for the gcc toolkit
MAINTAINER Aleix Pol <aleixpol@kde.org>

ARG QT_VERSION=6.4.0
ARG QT_TAG=v6.4.0
ARG NDK_VERSION=r22b
ARG SDK_PLATFORM=android-31
ARG SDK_BUILD_TOOLS=30.0.2
ARG MIN_NDK_PLATFORM=android-21
ARG SDK_PACKAGES="tools platform-tools"

ENV DEBIAN_FRONTEND noninteractive
ENV QT_PATH /opt/Qt
ENV ANDROID_HOME /opt/android-sdk
ENV ANDROID_SDK_ROOT ${ANDROID_HOME}
ENV ANDROID_NDK_ROOT /opt/android-ndk
ENV ANDROID_NDK_HOST linux-x86_64
ENV ANDROID_NDK_PLATFORM ${MIN_NDK_PLATFORM}
ENV ANDROID_API_VERSION ${SDK_PLATFORM}
ENV ANDROID_BUILD_TOOLS_REVISION ${SDK_BUILD_TOOLS}
ENV PATH ${QT_PATH}/bin:${ANDROID_HOME}/cmdline-tools/tools/bin:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:${PATH}

# Install updates & requirements:
#  * unzip - unpack platform tools
#  * git, openssh-client, ca-certificates - clone & build
#  * locales, sudo - useful to set utf-8 locale & sudo usage
#  * curl - to download Qt bundle
#  * make, openjdk-11-jdk - basic build requirements
RUN apt update && apt full-upgrade -y && apt install -y --no-install-recommends \
    unzip \
    git \
    openssh-client \
    ca-certificates \
    locales \
    sudo \
    curl \
    make \
    openjdk-11-jdk \
    build-essential \
    python3 \
    #build dependencies
    rdfind \
    python3-distutils \
    python3-future \
    python3-xdg \
    python3-requests \
    libxml-simple-perl \
    libjson-perl \
    libio-socket-ssl-perl \
    libyaml-perl libyaml-libyaml-perl \
    ninja-build \
    build-essential \
    gperf gettext \
    python3 python3-lxml python3-yaml \
    bison flex \
    ruby wget \
    #for freetype and libxml
    automake libtool autoconf autoconf-archive pkg-config \
    #for translations
    subversion gnupg2 \
    #for karchive
    zlib1g-dev \
    # for craft
    p7zip-full \
    python3-pip \
    python3-venv \
    # for qttools
    clang \
    libclang-dev \
    libclang-cpp-dev \
    llvm-dev \
    && apt-get -qq clean \
    && locale-gen en_US.UTF-8 && dpkg-reconfigure locales \
    && update-alternatives --install /usr/bin/python python /usr/bin/python3 10

# Install components needed by Gitlab CI
RUN pip install python-gitlab packaging

##########################

RUN chmod a+w /opt/

# Add group & user
RUN groupadd -r user && useradd --create-home --gid user user && echo 'user ALL=NOPASSWD: ALL' > /etc/sudoers.d/user

RUN mkdir /output
RUN chown user:user /output

USER user
WORKDIR /home/user
ENV HOME /home/user

##########################

# Download & unpack android SDK
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64
RUN curl -Lo /tmp/sdk-tools.zip 'https://dl.google.com/android/repository/commandlinetools-linux-6609375_latest.zip' \
    && mkdir -p /opt/android-sdk/cmdline-tools && unzip -q /tmp/sdk-tools.zip -d /opt/android-sdk/cmdline-tools && rm -f /tmp/sdk-tools.zip \
    && yes | sdkmanager --licenses && sdkmanager --verbose "platforms;${SDK_PLATFORM}" "build-tools;${SDK_BUILD_TOOLS}" ${SDK_PACKAGES} && sdkmanager --uninstall --verbose emulator

# Download & unpack android NDK
RUN mkdir /tmp/android && cd /tmp/android && curl -Lo ndk.zip "https://dl.google.com/android/repository/android-ndk-${NDK_VERSION}-linux-x86_64.zip" \
    && unzip -q ndk.zip && mv android-ndk-* $ANDROID_NDK_ROOT && chmod -R +rX $ANDROID_NDK_ROOT \
    && rm -rf /tmp/android \
    && rdfind -makehardlinks true -makeresultsfile false /opt/android-ndk

ENV ONLY_ARM64=1
COPY build-openssl /opt/helpers/
RUN bash /opt/helpers/build-openssl

RUN mkdir -p /opt/cmake \
    && curl -Lo /tmp/cmake.sh https://cmake.org/files/v3.19/cmake-3.19.1-Linux-x86_64.sh \
    && bash /tmp/cmake.sh --skip-license --prefix=/opt/cmake --exclude-subdir \
    && rm /tmp/cmake.sh
ENV PATH="/opt/cmake/bin:${PATH}"

# yes, "qt5" is the right repo for Qt6 as well...
RUN cd && git clone https://invent.kde.org/qt/qt/qt5.git --single-branch --branch ${QT_TAG} && \
    cd qt5 && \
    ./init-repository --module-subset=qtbase,qtshadertools,qtdeclarative,qtsvg,qt5compat,qtmultimedia,qtpositioning,qttools,qtwebsockets,qtcharts && \
    mkdir host && cd host && \
    ../configure -nomake tests -nomake examples -opensource -confirm-license -no-dbus -no-widgets -no-opengl -release -optimize-size -prefix /opt/nativetooling -skip qtsvg -skip qt5compat -skip qtpositioning -skip qtmultimedia -skip qtwebsockets -skip qtcharts && \
    cmake --build . --parallel && \
    cmake --install . && \
    cd .. && \
    mkdir target && cd target && \
    ../configure -platform android-clang -nomake tests -nomake examples -android-abis arm64-v8a -qt-host-path /opt/nativetooling -opensource -confirm-license -prefix $QT_PATH -android-sdk $ANDROID_SDK_ROOT -android-ndk $ANDROID_NDK_ROOT -skip qttools -- -DOPENSSL_ROOT_DIR=/opt/kdeandroid-arm64 && \
    cmake --build . --parallel && \
    cmake --install . && \
    cd .. && cd .. && rm -rf qt5

# must be set after the Qt 6 host build, otherwise configure fails there
ENV QT_HOST_PATH /opt/nativetooling

##########################

ENV ANDROID_NDK $ANDROID_NDK_ROOT
COPY gitconfig $HOME/.gitconfig

# makes sure gradle is downloaded, otherwise it will be downloaded every time
RUN sh $QT_PATH/src/3rdparty/gradle/gradlew -v

# # compile kf5 tooling
COPY build-cmake-native git-clone-macro.sh /opt/helpers/
ENV PERSIST=0
ARG KDE_CMAKE_HOST_ARGS="-DBUILD_WITH_QT6=ON -DEXCLUDE_DEPRECATED_BEFORE_AND_AT=5.91.0"
RUN /opt/helpers/build-cmake-native extra-cmake-modules kde:extra-cmake-modules -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DCMAKE_PREFIX_PATH="$QT_PATH;/opt/nativetooling" -DANDROID_ABI=arm64-v8a ${KDE_CMAKE_HOST_ARGS}
RUN /opt/helpers/build-cmake-native kconfig     kde:kconfig     -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DBUILD_SHARED_LIBS=OFF -DBUILD_TESTING=OFF -DCMAKE_PREFIX_PATH=/opt/nativetooling -DKCONFIG_USE_DBUS=OFF -DKCONFIG_USE_GUI=OFF ${KDE_CMAKE_HOST_ARGS}
RUN /opt/helpers/build-cmake-native kcoreaddons kde:kcoreaddons -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DBUILD_SHARED_LIBS=OFF -DBUILD_TESTING=OFF -DCMAKE_PREFIX_PATH=/opt/nativetooling -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Widgets=ON ${KDE_CMAKE_HOST_ARGS}

COPY build-autotools build-cmake build-qt build-eigen build-poppler build-vlcqt build-gstreamer-binaries create-apk get-apk-args.py \
     skip build-standalone build-discount 0001-Hack-Use-mktags-from-native-build-when-crosscompilin.patch craft-bootstrap craft-entrypoint.sh /opt/helpers/

ENV STANDALONE_EXTRA="--stl=libc++"

#build expat, needed for exiv2
RUN EXTRA_CMAKE_SUBDIR=expat /opt/helpers/build-cmake libexpat https://github.com/libexpat/libexpat.git

#build libical, needed for kcalendarcore
RUN GIT_EXTRA="--branch v3.0.10" /opt/helpers/build-cmake libical https://github.com/libical/libical -DICAL_GLIB=False -DLIBICAL_BUILD_TESTING=False

#build qrencode, needed fort prison
RUN GIT_EXTRA="--branch v4.1.1" /opt/helpers/build-cmake libqrencode https://github.com/fukuchi/libqrencode.git -DWITH_TOOLS=OFF

#build taglib, required for kasts
RUN GIT_EXTRA="--branch v1.12" /opt/helpers/build-cmake taglib https://github.com/taglib/taglib

#build zxing-cpp, needed for kitinerary, qrca
RUN GIT_EXTRA="--branch v1.2.0" /opt/helpers/build-cmake zxing-cpp https://github.com/nu-book/zxing-cpp.git -DBUILD_UNIT_TESTS=OFF -DBUILD_BLACKBOX_TESTS=OFF -DBUILD_EXAMPLES=OFF

# needed for Neochat
RUN GIT_EXTRA="--branch 0.30.2" /opt/helpers/build-cmake cmark https://github.com/commonmark/cmark.git

# TODO pending Qt 6 support
# RUN GIT_EXTRA='--branch=0.6.11' /opt/helpers/build-cmake libquotient https://github.com/quotient-im/libquotient.git
#
# RUN GIT_EXTRA='--branch=v0.13.1' /opt/helpers/build-cmake qtkeychain https://github.com/frankosterfeld/qtkeychain.git
#
# RUN GIT_EXTRA='--branch=v0.2.0' /opt/helpers/build-cmake qcoro https://github.com/danvratil/qcoro -DQCORO_WITH_QTDBUS=OFF -DBUILD_TESTING=OFF

# needed for Koko
RUN GIT_EXTRA="--branch=v0.27.4" /opt/helpers/build-cmake exiv2 https://github.com/exiv2/exiv2 -DCMAKE_POSITION_INDEPENDENT_CODE=On -DEXIV2_BUILD_EXIV2_COMMAND=Off -DEXIV2_BUILD_SAMPLES=Off

# Needed for Kaidan
# TODO pending Qt 6 support
# RUN GIT_EXTRA="--branch=v1.4.0" /opt/helpers/build-cmake qxmpp https://github.com/qxmpp-project/qxmpp.git

# Needed for Itinerary, Okular
RUN /opt/helpers/build-poppler

# Needed for itinerary
RUN GIT_EXTRA="--branch=v2.9.12" AUTOTOOLS_EXTRA='--disable-static --without-lzma --without-python --without-debug --without-docbook --without-http' /opt/helpers/build-autotools libxml2 https://gitlab.gnome.org/GNOME/libxml2.git

# needs to be after building qt, otherwise it breaks weirdly
ENV QMAKESPEC android-clang

# Needed for KI18n
RUN /opt/helpers/build-cmake libintl-lite https://github.com/j-jorge/libintl-lite.git
ENV PATH ${PATH}:/opt/helpers

ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
ENV PERSIST=1

# Gitlab CI specific setup
USER user
RUN export >> /home/user/.profile
